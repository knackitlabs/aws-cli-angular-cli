FROM node:12

RUN apt-get update && \
	apt-get install -y python python-pip python-dev

RUN npm install -g \
	@angular/cli

RUN pip install awscli